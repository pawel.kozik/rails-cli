import { Command } from "commander";
import { getConfig } from "../get-config";
import { pm2 } from "../helpers/pm2";

const { railsCoreProjectPath } = getConfig();

const serviceName = "railsCore";

const upAsync = () =>
  pm2.start(railsCoreProjectPath, serviceName, "npm run start:dev");

const down = () => pm2.del(serviceName);

export const core = () => {
  const commandDefinition = new Command("core");

  commandDefinition.alias("c");
  commandDefinition.command("up").action(upAsync);
  commandDefinition.command("down").action(down);

  return commandDefinition;
};
