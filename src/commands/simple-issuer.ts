import { Command } from "commander";
import { getConfig } from "../get-config";
import { pm2 } from "../helpers/pm2";

const { simpleIssuerProjectPath } = getConfig();

const serviceName = "simpleIssuer";

const up = () => {
  if (!simpleIssuerProjectPath) {
    throw new Error("Cannot up simple issuer - path is not configured");
  }

  pm2.start(simpleIssuerProjectPath, serviceName, "npm run start");
};

const down = () => pm2.del(serviceName);

export const simpleIssuer = () => {
  const commandDefinition = new Command("simple-issuer");

  commandDefinition.alias("si");
  commandDefinition.command("up").action(up);
  commandDefinition.command("down").action(down);

  return commandDefinition;
};
