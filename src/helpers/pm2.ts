import { execCommand } from "./exec-command";

const start = (path: string, serviceName: string, command: string) =>
  execCommand(`cd ${path} ; pm2 start "${command}" --name ${serviceName}`);

const del = (serviceName: string) => execCommand(`pm2 delete ${serviceName}`);

export const pm2 = {
  start,
  del,
};
