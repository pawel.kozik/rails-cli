import { exec } from "node:child_process";

export const execCommand = (command: string) => {
  exec(command, (err: any, output: any) => {
    if (err) {
      // log and return if we encounter an error
      console.error("could not execute command: ", err);
      return;
    }
    // log the output received from the command
    console.log("Output: \n", output);
  });
};
