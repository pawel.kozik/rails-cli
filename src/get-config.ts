import { config } from "../config";

export interface ConfigInterface {
  railsCoreProjectPath: string;
  simpleIssuerProjectPath?: string;
}

export const getConfig = (): ConfigInterface => {
  return config;
};
