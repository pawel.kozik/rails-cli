#! /usr/bin/env node

import { Command } from "commander";
import { core } from "./commands/core";
import { simpleIssuer } from "./commands/simple-issuer";

const program = new Command();

program
  .version("0.0.1")
  .description("Rails common operation")
  .addCommand(core())
  .addCommand(simpleIssuer())
  .parse(process.argv);
