# Rails-cli

## Dependencies

You need pm2 lib:

```shell
npm install pm2 -g
```

## Config

Add a new file `config.ts` in the root of project:

```typescript
import { ConfigInterface } from "./src/get-config";

export const config: ConfigInterface = {
  railsCoreProjectPath: "<your-path>",
};
```

## Install

```shell
npm run update
```
